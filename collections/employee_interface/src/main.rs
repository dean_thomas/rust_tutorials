use std::collections::HashMap;
use std::io::{self, Write};


fn print_employees(value: &HashMap<String, Vec<String>>) -> ()
{
    for (k, vs) in value.iter() {
        let names = vs.join(", ");
        println!("{}: {}", k, names);
        // for v in vs {
        //     print!(" {}", v);
        // }
        // println!();
    }
}

fn main() {
    println!("Hello, world!");

    let mut lookup = HashMap::new();

    loop {
        let mut d = String::new();
        let mut n = String::new();
        
        print!("Enter name:");
        io::stdout().flush().unwrap();
        let name = match io::stdin().read_line(&mut n) {
            Ok(_) => String::from(n.trim()),
            Err(_) => continue
        };
        
        print!("Enter department:");
        io::stdout().flush().unwrap();
        let dept = match io::stdin().read_line(&mut d) {
            Ok(_) => String::from(d.trim()),
            Err(_) => continue
        };
        
        lookup.entry(dept).or_insert(vec![]).push(name);
        print_employees(&lookup);
    }
}
