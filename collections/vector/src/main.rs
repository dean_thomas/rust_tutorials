use std::collections::HashMap;


fn main() {
    let values = vec![1, 2, 3, 4, 5, 6, 5, 2, 3, 1, 1, 1, 2, 4];

    println!("sum: {}", sum(&values));
    println!("mean: {}", mean(&values));
    println!("median: {}", median(&values));
    println!("mode: {}", mode(&values));
}

fn median(values: &[i32]) -> f32 {
    //  sort a copy of the vector
    let mut sorted = values.to_vec();
    sorted.sort();
    let length = sorted.len();
    let mid = length / 2;
    let mid_point = mid;
    sorted[mid_point] as f32
}

fn mean(values: &[i32]) -> f32 {
    sum(&values) as f32  / values.len() as f32
}

fn count_values(values: &[i32]) -> HashMap<i32, i32> {
    let mut result = HashMap::new();
    for &n in values {
        *result.entry(n).or_insert(0) += 1;
    }
    result
}

fn mode(values: &[i32]) -> f32 {
    //  
    let counted = count_values(values);
    // for (key, value) in &counted {
    //     println!("{}: {}", key, value);
    // }
    
    let (k,_) = counted.iter().max_by(|x, y| y.cmp(x)).unwrap();
    *k as f32
}

fn sum(values: &[i32]) -> i32 { 
    values.iter().sum::<i32>()
}
