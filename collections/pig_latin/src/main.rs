fn main() {
    println!("{}", to_pig_latin(&String::from("apple")));
    println!("{}", to_pig_latin(&String::from("first")));
    println!("{}", to_pig_latin(&String::from("")));
}


fn swap_vowel(value: &str) -> String {
    //  Words that start with a vowel have “hay” added to the end 
    //  instead (“apple” becomes “apple-hay”)
    let mut result = String::from(value);
    result.push_str("-hay");
    
    result
}

fn swap_consonant(value: &str) -> String {
    //  The first consonant of each word is moved to the end of the word 
    //  and “ay” is added, so “first” becomes “irst-fay.”
    let mut result = String::from(value);
    let c = result.remove(0);
    result.push('-');
    result.push(c);
    result.push_str("ay");
    
    result
}

fn to_pig_latin(value: &str) -> String {
    //  Convert strings to pig latin. The first consonant of each word
    //  is moved to the end of the word and “ay” is added, so “first” 
    //  becomes “irst-fay.” Words that start with a vowel have “hay” 
    //  added to the end instead (“apple” becomes “apple-hay”). Keep in 
    //  mind the details about UTF-8 encoding!
    match value.chars().nth(0) {
        Some(c) => match c {
            'a' | 'e' | 'i' | 'o' | 'u' => swap_vowel(&value),
            _ => swap_consonant(&value)

        },
        None => panic!("Unable to parse '{}'", value),
    }
}
